package basketball;

import java.util.Arrays;
import java.util.List;

public class basketball04 {
  public static void main(String[] args) {
    List<Club> table = Arrays.asList(
        new Club(1, "Los Angeles Lakers", 22, 16, 1, 5, 1200, 950, 250, 350, 200,
            80, 20, 76, 300, 150, 50, 100, 30),
        new Club(2, "Golden State Warriors", 22, 16, 0, 6, 1300, 800, 500, 300, 150,
            90, 30, 75, 320, 180, 80, 120, 40),
        new Club(3, "Brooklyn Nets", 22, 15, 1, 6, 1100, 900, 200, 250, 220,
            60, 30, 68, 280, 160, 50, 100, 20),
        new Club(4, "Los Angeles Clippers", 22, 14, 1, 7, 1400, 1000, 400, 300, 180,
            50, 50, 68, 340, 220, 120, 200, 70),
        new Club(5, "Houston Rockets", 22, 14, 0, 8, 1200, 900, 300, 250, 200,
            70, 30, 68, 310, 190, 80, 150, 50),
        new Club(6, "Boston Celtics", 22, 11, 2, 9, 1300, 1100, 200, 350, 240, 90, 40,
            61, 380, 220, 90, 150, 60),
        new Club(7, "Toronto Raptors", 22, 11, 0, 11, 1000, 950, 50, 300, 200,
            60, 40, 54, 330, 180, 70, 120, 30),
        new Club(8, "Philadelphia 76ers", 22, 10, 0, 12, 900, 1100, -200, 200, 180,
            40, 50, 49, 300, 190, 80, 130, 40),
        new Club(9, "Denver Nuggets", 22, 9, 1, 12, 1100, 1150, -50, 350, 250,
            60, 30, 48, 350, 220, 90, 150, 30),
        new Club(10, "Dallas Mavericks", 22, 7, 1, 14, 900, 1150, -250, 300, 200,
            60, 30, 40, 320, 180, 70, 120, 30),
        new Club(11, "Utah Jazz", 22, 5, 1, 16, 950, 1050, -100, 400, 250,
            60, 80, 34, 370, 200, 90, 200, 50),
        new Club(12, "Miami Heat", 22, 0, 0, 22, 800, 1300, -500, 100, 450, 10,
            0, 1, 0, 0, 0, 0, 0));

    System.out.println("Several clubs have 68 points");
    table.stream().filter(club -> club.getPoints() == 68)
        .forEach(System.out::println);

    System.out.println();
    System.out.println("Clubs with 73 points");
    table.stream().filter(club -> club.getPoints() == 73)
        .forEach(System.out::println);

  }
}

