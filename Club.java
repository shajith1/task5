package basketball;

public class Club implements Comparable<Club> {
    private int position;
    private String teamName;
    private int playedGames;
    private int wonGames;
    private int drawnGames;
    private int lostGames;
    private int pointsFor;
    private int pointsAgainst;
    private int pointsDifference;
    private int fieldGoalsMade;
    private int fieldGoalsAttempted;
    private int threePointersMade;
    private int threePointersAttempted;
    private int freeThrowsMade;
    private int freeThrowsAttempted;
    private int steals;
    private int blocks;
    private int turnovers;
    private int points;

    public Club(int position, String teamName, int playedGames, int wonGames,
                int drawnGames, int lostGames, int pointsFor, int pointsAgainst,
                int pointsDifference, int fieldGoalsMade, int fieldGoalsAttempted,
                int threePointersMade, int threePointersAttempted,
                int freeThrowsMade, int freeThrowsAttempted, int steals,
                int blocks, int turnovers, int points) {
        this.position = position;
        this.teamName = teamName;
        this.playedGames = playedGames;
        this.wonGames = wonGames;
        this.drawnGames = drawnGames;
        this.lostGames = lostGames;
        this.pointsFor = pointsFor;
        this.pointsAgainst = pointsAgainst;
        this.pointsDifference = pointsDifference;
        this.fieldGoalsMade = fieldGoalsMade;
        this.fieldGoalsAttempted = fieldGoalsAttempted;
        this.threePointersMade = threePointersMade;
        this.threePointersAttempted = threePointersAttempted;
        this.freeThrowsMade = freeThrowsMade;
        this.freeThrowsAttempted = freeThrowsAttempted;
        this.steals = steals;
        this.blocks = blocks;
        this.turnovers = turnovers;
        this.points = points;
    }

    public String toString() {
        return String.format("%-3d%-20s%10d%10d%10d", position, teamName, pointsFor,
                pointsAgainst, points);
    }

    public int compareTo(Club t) {
        return Integer.compare(pointsFor, t.pointsFor);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public void setPlayedGames(int playedGames) {
        this.playedGames = playedGames;
    }

    public int getWonGames() {
        return wonGames;
    }

    public void setWonGames(int wonGames) {
        this.wonGames = wonGames;
    }

    public int getDrawnGames() {
        return drawnGames;
    }

    public void setDrawnGames(int drawnGames) {
        this.drawnGames = drawnGames;
    }

    public int getLostGames() {
        return lostGames;
    }

    public void setLostGames(int lostGames) {
        this.lostGames = lostGames;
    }

    public int getPointsFor() {
        return pointsFor;
    }

    public void setPointsFor(int pointsFor) {
        this.pointsFor = pointsFor;
    }

    public int getPointsAgainst() {
        return pointsAgainst;
    }

    public void setPointsAgainst(int pointsAgainst) {
        this.pointsAgainst = pointsAgainst;
    }

    public int getPointsDifference() {
        return pointsDifference;
    }

    public void setPointsDifference(int pointsDifference) {
        this.pointsDifference = pointsDifference;
    }

    public int getFieldGoalsMade() {
        return fieldGoalsMade;
    }

    public void setFieldGoalsMade(int fieldGoalsMade) {
        this.fieldGoalsMade = fieldGoalsMade;
    }

    public int getFieldGoalsAttempted() {
        return fieldGoalsAttempted;
    }

    public void setFieldGoalsAttempted(int fieldGoalsAttempted) {
        this.fieldGoalsAttempted = fieldGoalsAttempted;
    }

    public int getThreePointersMade() {
        return threePointersMade;
    }

    public void setThreePointersMade(int threePointersMade) {
        this.threePointersMade = threePointersMade;
    }

    public int getThreePointersAttempted() {
        return threePointersAttempted;
    }

    public void setThreePointersAttempted(int threePointersAttempted) {
        this.threePointersAttempted = threePointersAttempted;
    }

    public int getFreeThrowsMade() {
        return freeThrowsMade;
    }

    public void setFreeThrowsMade(int freeThrowsMade) {
        this.freeThrowsMade = freeThrowsMade;
    }

    public int getFreeThrowsAttempted() {
        return freeThrowsAttempted;
    }

    public void setFreeThrowsAttempted(int freeThrowsAttempted) {
        this.freeThrowsAttempted = freeThrowsAttempted;
    }

    public int getSteals() {
        return steals;
    }

    public void setSteals(int steals) {
        this.steals = steals;
    }

    public int getBlocks() {
        return blocks;
    }

    public void setBlocks(int blocks) {
        this.blocks = blocks;
    }

    public int getTurnovers() {
        return turnovers;
    }

    public void setTurnovers(int turnovers) {
        this.turnovers = turnovers;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

	

	
}
