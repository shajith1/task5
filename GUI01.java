package gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;



public class GUI01 {

  public static void main(String[] args) {
    JFrame f = new JFrame();
    JButton b = new JButton("Press Me");
    f.setLayout(new BorderLayout());
    f.add(b, BorderLayout.CENTER);
    
    b.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        System.out.println("Button says: " + 
          event.getActionCommand());        
      }
    });
        
    f.pack();
    f.setVisible(true);
  }
}
