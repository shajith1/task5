package gui;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class GUI03 extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTable table;
    private JLabel detailsLabel;
    private JButton showDetailsButton;

    public GUI03(List<Club> clubList) {
        setTitle("Basketball Club Standings");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 400);

        // Create a table model
        BasketballTableModel model = new BasketballTableModel(clubList);

        // Create a JTable with the custom model
        table = new JTable(model);

        // Set up the table with scroll pane
        JScrollPane scrollPane = new JScrollPane(table);
        getContentPane().add(scrollPane, BorderLayout.CENTER);

        // Create a panel for details
        JPanel detailsPanel = new JPanel(new FlowLayout());
        detailsLabel = new JLabel("Select a row to view details");
        showDetailsButton = new JButton("Show Details");
        showDetailsButton.addActionListener(e -> showDetails());
        detailsPanel.add(detailsLabel);
        detailsPanel.add(showDetailsButton);

        // Add the details panel to the frame
        getContentPane().add(detailsPanel, BorderLayout.SOUTH);

        // Display the frame
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void showDetails() {
        int selectedRow = table.getSelectedRow();
        if (selectedRow != -1) {
            Club selectedClub = ((BasketballTableModel) table.getModel()).getClub(selectedRow);
            detailsLabel.setText("Details for " + selectedClub.getTeamName() +
                    ": Points For - " + selectedClub.getPointsFor() +
                    ", Points Against - " + selectedClub.getPointsAgainst() +
                    ", Points - " + selectedClub.getPoints());
        } else {
            detailsLabel.setText("No row selected. Select a row to view details.");
        }
    }

    public static void main(String[] args) {
        List<Club> table = Arrays.asList(
        		new Club(1, "Northampton Saints", 22, 16, 1, 5, 621, 400, 221, 75, 41,
        		        8, 2, 76, 20, 15, 10, 5, 3),
        		new Club(2, "Manchester Tigers", 22, 15, 2, 5, 600, 410, 190, 72, 38,
        		        10, 3, 70, 18, 14, 8, 4, 2),
        		// Add other Club objects here
        		new Club(3, "Birmingham Bears", 22, 14, 3, 5, 590, 420, 170, 70, 40,
        		        9, 2, 72, 15, 12, 7, 3, 1),
        		new Club(4, "London Lions", 22, 13, 4, 5, 580, 430, 150, 68, 35,
        		        12, 3, 75, 12, 10, 6, 2, 1),
        		new Club(5, "Liverpool Leopards", 22, 12, 5, 5, 570, 440, 130, 65, 42,
        		        7, 2, 78, 22, 18, 10, 4, 2),
        		new Club(6, "Leeds Rhinos", 22, 11, 6, 5, 560, 450, 110, 63, 39,
        		        11, 4, 73, 20, 16, 9, 3, 1),
        		new Club(7, "Edinburgh Eagles", 22, 10, 7, 5, 550, 460, 90, 60, 37,
        		        10, 2, 74, 18, 14, 8, 2, 1),
        		new Club(8, "Glasgow Giants", 22, 9, 8, 5, 540, 470, 70, 58, 34,
        		        9, 3, 75, 15, 12, 7, 3, 1),
        		new Club(9, "Cardiff Cobras", 22, 8, 9, 5, 530, 480, 50, 55, 32,
        		        8, 2, 76, 14, 11, 6, 2, 1),
        		new Club(10, "Bristol Bulldogs", 22, 7, 10, 5, 520, 490, 30, 53, 30,
        		        7, 3, 77, 16, 13, 7, 3, 1),
        		new Club(11, "Oxford Owls", 22, 6, 11, 5, 510, 500, 10, 50, 28,
        		        6, 2, 78, 10, 8, 5, 2, 1),
        		new Club(12, "London Welsh", 22, 0, 0, 22, 223, 1021, -798, 29, 147, 1,
        		        0, 1, 0, 0, 0, 0, 0));


        SwingUtilities.invokeLater(() -> new GUI03(table));
    }
}

class BasketballTableModel extends AbstractTableModel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Club> clubList;
    private String[] columnNames = {"Position", "Team Name", "Points For", "Points Against", "Points"};

    public BasketballTableModel(List<Club> clubList) {
        this.clubList = clubList;
    }

    @Override
    public int getRowCount() {
        return clubList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Club club = clubList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return club.getPosition();
            case 1:
                return club.getTeamName();
            case 2:
                return club.getPointsFor();
            case 3:
                return club.getPointsAgainst();
            case 4:
                return club.getPoints();
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    public Club getClub(int rowIndex) {
        return clubList.get(rowIndex);
    }
}

class Club {
    private int position;
    private String teamName;
    private int playedGames;
    private int wonGames;
    private int drawnGames;
    private int lostGames;
    private int pointsFor;
    private int pointsAgainst;
    private int pointsDifference;
    private int fieldGoalsMade;
    private int fieldGoalsAttempted;
    private int threePointersMade;
    private int threePointersAttempted;
    private int freeThrowsMade;
    private int freeThrowsAttempted;
    private int steals;
    private int blocks;
    private int turnovers;
    private int points;

    public Club(int position, String teamName, int playedGames, int wonGames, int drawnGames, int lostGames,
                int pointsFor, int pointsAgainst, int pointsDifference, int fieldGoalsMade, int fieldGoalsAttempted,
                int threePointersMade, int threePointersAttempted,
                int freeThrowsMade, int freeThrowsAttempted, int steals,
                int blocks, int turnovers, int points) {
        this.position = position;
        this.teamName = teamName;
        this.playedGames = playedGames;
        this.wonGames = wonGames;
        this.drawnGames = drawnGames;
        this.lostGames = lostGames;
        this.pointsFor = pointsFor;
        this.pointsAgainst = pointsAgainst;
        this.pointsDifference = pointsDifference;
        this.fieldGoalsMade = fieldGoalsMade;
        this.fieldGoalsAttempted = fieldGoalsAttempted;
        this.threePointersMade = threePointersMade;
        this.threePointersAttempted = threePointersAttempted;
        this.freeThrowsMade = freeThrowsMade;
        this.freeThrowsAttempted = freeThrowsAttempted;
        this.steals = steals;
        this.blocks = blocks;
        this.turnovers = turnovers;
        this.points = points;
    }

    public int getPosition() {
        return position;
    }

    public String getTeamName() {
        return teamName;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public int getWonGames() {
        return wonGames;
    }

    public int getDrawnGames() {
        return drawnGames;
    }

    public int getLostGames() {
        return lostGames;
    }

    public int getPointsFor() {
        return pointsFor;
    }

    public int getPointsAgainst() {
        return pointsAgainst;
    }

    public int getPointsDifference() {
        return pointsDifference;
    }

    public int getFieldGoalsMade() {
        return fieldGoalsMade;
    }

    public int getFieldGoalsAttempted() {
        return fieldGoalsAttempted;
    }

    public int getThreePointersMade() {
        return threePointersMade;
    }

    public int getThreePointersAttempted() {
        return threePointersAttempted;
    }

    public int getFreeThrowsMade() {
        return freeThrowsMade;
    }

    public int getFreeThrowsAttempted() {
        return freeThrowsAttempted;
    }

    public int getSteals() {
        return steals;
    }

    public int getBlocks() {
        return blocks;
    }

    public int getTurnovers() {
        return turnovers;
    }

    public int getPoints() {
        return points;
    }
}